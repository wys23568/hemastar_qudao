export default [
  // admin
  {
    path: '/admin',
    component: '../layouts/UserLayout',
    routes: [
      { path: '/admin', redirect: '/admin/login' },
      { path: '/admin/login', name: 'login', component: './Admin/Login' },
      { path: '/admin/register', name: 'register', component: './Admin/Register' },
      {
        path: '/admin/register-result',
        name: 'register.result',
        component: './Admin/RegisterResult',
      },
      { component: '404' },
    ],
  },
  // user
  {
    path: '/user',
    component: '../layouts/UserLayout',
    routes: [
      { path: '/user', redirect: '/user/login' },
      { path: '/user/login', name: 'login', component: './User/Login' },
      { path: '/user/register', name: 'register', component: './User/Register' },
      {
        path: '/user/register-result',
        name: 'register.result',
        component: './User/RegisterResult',
      },
      {
        component: '404',
      },
    ],
  },
  // app
  {
    path: '/',
    component: '../layouts/BasicLayout',
    Routes: ['src/pages/Authorized'],
    routes: [
      { path: '/', redirect: '/index', authority: ['admin', 'user'] },
      { path: '/index', icon: 'read', name: 'course', component: './Home' },
      {
        component: '404',
      },
    ],
  },
];
