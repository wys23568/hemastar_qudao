import { query as queryUsers } from '@/services/user';
import { haimawangApi } from '@/services/service';
import { Layout, message } from 'antd';

export default {
  namespace: 'user',

  state: {
    list: [],
    currentUser: {},
  },

  effects: {
    *fetch(_, { call, put }) {
      const response = yield call(queryUsers);
      yield put({
        type: 'save',
        payload: response,
      });
    },
    *fetchCurrent(_, { call, put }) {
      const url = 'account/channel/info';
      const data = {};
      const response = yield call(haimawangApi, { url, data });
      if (response.result === true) {
        const user = {};
        user.avatar = response.data.avatarUrl;
        user.name = response.data.name;
        user.phone = response.data.phone;
        user.email = response.data.email;
        yield put({
          type: 'saveCurrentUser',
          payload: user,
        });
      } else{
        message.error("当前用户没有权限，请联系相关人员")
        window.g_app._store.dispatch({
          type: 'login/logout',
        });
      }
    },
  },

  reducers: {
    save(state, action) {
      return {
        ...state,
        list: action.payload,
      };
    },
    saveCurrentUser(state, action) {
      return {
        ...state,
        currentUser: action.payload || {},
      };
    },
    changeNotifyCount(state, action) {
      return {
        ...state,
        currentUser: {
          ...state.currentUser,
          notifyCount: action.payload.totalCount,
          unreadCount: action.payload.unreadCount,
        },
      };
    },
  },
};
