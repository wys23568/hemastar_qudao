import React, { Component, Fragment } from 'react';
import { Button } from 'antd';
import styles from './index.less';
import marginRight from 'antd/es/tag';

const ButtonGroup = Button.Group;

class ServiceButton extends Component {
  stateSearch = state => {
    const { handleStateSearch } = this.props;
    handleStateSearch(state);
  };

  render() {
    const { searchState, stateFilters } = this.props;

    const stateButtons = [];
    if (stateFilters && stateFilters.length > 0) {
      stateButtons.push(
        <Button
          style={{ marginRight: 0 }}
          key="-1"
          type={searchState === -1 ? 'primary' : ''}
          onClick={() => this.stateSearch(-1)}
        >
          全部
        </Button>
      );
      stateFilters.map(item =>
        stateButtons.push(
          <Button
            style={{ marginRight: 0 }}
            key={item.value}
            type={searchState === item.value ? 'primary' : ''}
            onClick={() => this.stateSearch(item.value)}
          >
            {item.text}
          </Button>
        )
      );
    }
    return (
      <Fragment>
        <ButtonGroup className={styles.tableListStateBG}>{stateButtons}</ButtonGroup>
        <div style={{ clear: 'both' }} />
      </Fragment>
    );
  }
}

export default ServiceButton;
