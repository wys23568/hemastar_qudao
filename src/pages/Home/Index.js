import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import moment from 'moment';
import { Row, Col, Card, Form, } from 'antd';
import ServiceTable from '@/component/ServiceTable';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';

import styles from './Index.less';

@connect(({ service, loading }) => ({
  service,
  loading: loading.models.service,
}))
@Form.create()
class FeedbackList extends PureComponent {
  state = {
    searchFormValues: {},
    searchPageList: {
      content: [],
      pageable: {},
    },
  };
  searchParams = {};
  searchState = -1;
  columns = [
    {
      title: '序号',
      dataIndex: 'id',
      render: (val,record,index,) => {
        return <span>{(index+1)+(this.state.searchPageList.pageable.pageNumber)*10}</span>
      },
    },
    {
      title: '姓名',
      dataIndex: 'name',
      render: val => <span>{val}</span>,
    },
    {
      title: '性别',
      dataIndex: 'gender',
      render: val => {
        if (val == 0) {
          return <span>未知</span>;
        } else if (val == 1) {
          return <span>男</span>;
        } else if (val == 2) {
          return <span>女</span>;
        }
      },
    },
    {
      title: '年龄',
      dataIndex: 'age',
      render: val => <span>{val}</span>,
    },
    {
      title: '手机号',
      dataIndex: 'phone',
      render: val => <span>{val}</span>
    },
    {
      title: '提交时间',
      dataIndex: 'date',
      render: val => <span>{moment(val).format('YYYY-MM-DD HH:mm:ss')}</span>
    },
  ];

  componentDidMount() {
    this.beginSearch({ pageTotal: 0 });
  }

  handleStateSearch = state => {
    this.searchState = state;
    this.beginSearch({ pageTotal: 0 });
  };

  beginSearch = params => {
    const { dispatch } = this.props;
    params.searchState = this.searchState;
    this.searchParams = params;
    dispatch({
      type: 'service/haimawangApiCall',
      payload: { url: `account/channel/user/list`, data: params },
      callback: response => {
        if (response.result) {
          this.setState({
            searchPageList: response.data,
          });
        } else {
          this.setState({
            searchPageList: {},
          });
        }
      },
    });
  };

  handleServiceTableSearchChange = (pagination, filtersArg, sorter) => {
    const { searchFormValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      pageNumber: pagination.current - 1,
      pageSize: pagination.pageSize,
      pageTotal: pagination.total,
      ...searchFormValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    this.beginSearch(params);
  };

  handleSelectRows = rows => {
    this.setState({
      selectedRows: rows,
    });
  };
  render() {
    const { loading } = this.props;
    const { searchPageList } = this.state;

    return (
      <PageHeaderWrapper title="活动参与者">
        <Card bordered={false}>
          <div className={styles.tableList}>
            <ServiceTable
              selectedRows={[]}
              loading={loading}
              data={searchPageList}
              columns={this.columns}
              onChangeSearch={this.handleServiceTableSearchChange}
            />
          </div>
        </Card>
      </PageHeaderWrapper>
    );
  }
}

export default FeedbackList;
